﻿using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Types;


namespace Smartwyre.DeveloperTest.Services.RebateImplementations
{
    public class RebateIncentiveByAmountPerUom: IRebateService
    {
        public CalculateRebateResult Calculate(CalculateRebateRequest request, Rebate rebate, Product product)
        {
            var result = new CalculateRebateResult();
            var rebateAmount = 0m;

            result.Success = IsResultSuccessByEmptyValues(rebate,product,request.Volume) ? false :
                             (!product.SupportedIncentives.HasFlag(SupportedIncentiveType.AmountPerUom)) ? false :
                             rebate.Amount == 0 ? false : true;

            rebateAmount = rebateAmount += rebate.Amount * request.Volume;

            if (result.Success)
            {
                var storeRebateDataStore = new RebateDataStore();
                storeRebateDataStore.StoreCalculationResult(rebate, rebateAmount);
            }

            return result;
        }

        private bool IsResultSuccessByEmptyValues(Rebate rebate, Product product, decimal requestVolume)
        {
            if (rebate == null || product == null || rebate.Amount == 0 || requestVolume == 0)
            {
                return false;
            }
            return true;
        }
    }
}
