﻿using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services.RebateImplementations
{
    public class RebateIncentiveByFixedCashAmount: IRebateService
    {
        public CalculateRebateResult Calculate(CalculateRebateRequest request, Rebate rebate, Product product) 
        {       
            var result = new CalculateRebateResult();
            var rebateAmount = 0m;
            
            result.Success = rebate == null ?  false:
                            (!product.SupportedIncentives.HasFlag(SupportedIncentiveType.FixedCashAmount)) ? false: 
                            rebate.Amount == 0 ? false: true;

            rebateAmount = rebate.Amount;

            if (result.Success)
            {
                var storeRebateDataStore = new RebateDataStore();
                storeRebateDataStore.StoreCalculationResult(rebate, rebateAmount);
            }

            return result;
        }
    }
}
