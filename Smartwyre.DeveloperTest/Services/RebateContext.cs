﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smartwyre.DeveloperTest.Services
{
    public class RebateContext
    {
        private IRebateService _rebateService;

        public RebateContext(IRebateService rebateService)
        {
            this._rebateService = rebateService;
        }

        public void SetRebateServiceIncentive(IRebateService rebateService)
        {
            this._rebateService = rebateService;
        }
    }
}
