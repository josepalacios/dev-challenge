﻿using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services;

public class RebateService : IRebateService
{
    private readonly IRebateService _rebateService;
    
    public RebateService(IRebateService rebateService)
    {
        _rebateService = rebateService;
    }

    public CalculateRebateResult Calculate(CalculateRebateRequest request, Rebate rebate, Product product)
    {
        var rebateDataStore = new RebateDataStore();
        var productDataStore = new ProductDataStore();

        rebate = rebateDataStore.GetRebate(request.RebateIdentifier);
        product = productDataStore.GetProduct(request.ProductIdentifier);

        return _rebateService?.Calculate(request, rebate, product);
    }
}
