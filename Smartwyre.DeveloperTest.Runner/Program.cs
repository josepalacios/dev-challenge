﻿using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Services.RebateImplementations;
using System;

namespace Smartwyre.DeveloperTest.Runner;

class Program
{
    static void Main(string[] args)
    {

        var rebateContext = new RebateContext(new RebateIncentiveByAmountPerUom());
        Console.WriteLine("Select an option to calculate rebate");
        Console.WriteLine("1. By Amount Per Uom");
        Console.WriteLine("2. By Fixed Cash Amount");
        Console.WriteLine("3. By Fixed Rate Rebate");
        string selectedOption = Console.ReadLine();

        if (selectedOption == "1")
        {
            rebateContext.SetRebateServiceIncentive(new RebateIncentiveByAmountPerUom());
        }
        if (selectedOption == "2")
        {
            rebateContext.SetRebateServiceIncentive(new RebateIncentiveByFixedCashAmount());
        }
        else
        {
            rebateContext.SetRebateServiceIncentive(new RebateIncentiveByFixedRateRebate());
        }
    }
}
