using FakeItEasy;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Services.RebateImplementations;
using Smartwyre.DeveloperTest.Types;
using System;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests;

public class PaymentServiceTests
{
    private readonly RebateService _rebateService;
    private readonly RebateContext _rebateContext;

    public PaymentServiceTests()
    {
        //Dependencies
        _rebateContext = A.Fake<RebateContext>();
        _rebateService = A.Fake<RebateService>();
    }
    [Fact]
    public void RebateService_Calculate_IncentiveByAmountPerUomIsSuccess()
    {
        //Arrange
        var rebateIncentiveByAmountPerUom = A.Fake<RebateIncentiveByAmountPerUom>();
        _rebateContext.SetRebateServiceIncentive(rebateIncentiveByAmountPerUom);


        var calculateRebateRequest = A.Fake<CalculateRebateRequest>();
        var rebate = A.Fake<Rebate>();
        var product = A.Fake<Product>();
        var rebateResult = A.Fake < CalculateRebateResult>();
        rebateResult.Success = true;

        //Act
        A.CallTo(() => _rebateService.Calculate(calculateRebateRequest, rebate, product)).Returns(rebateResult);
        //Assert

        Assert.True(rebateResult.Success);
    }

    [Fact]
    public void RebateService_Calculate_IncentiveByAmountPerUomIsCalled()
    {
        //Arrange
        var rebateIncentiveByAmountPerUom = A.Fake<RebateIncentiveByAmountPerUom>();
        _rebateContext.SetRebateServiceIncentive(rebateIncentiveByAmountPerUom);


        var calculateRebateRequest = A.Fake<CalculateRebateRequest>();
        var rebate = A.Fake<Rebate>();
        var product = A.Fake<Product>();
        var rebateResult = A.Fake<CalculateRebateResult>();
        rebateResult.Success = true;

        //Act
        A.CallTo(() => _rebateService.Calculate(calculateRebateRequest, rebate, product)).Returns(rebateResult);
        //Assert

        A.CallTo(() => _rebateService.Calculate(calculateRebateRequest, rebate, product)).MustHaveHappened();

    }
}
